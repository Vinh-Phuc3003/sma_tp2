import json
import random
from datetime import datetime

from pygame import Vector2, time
from fustrum import Fustrum
import core

COLOR_SUPERPREDATEUR = (220, 20, 60)
COLOR_CARNIVORE = (50, 205, 50)
COLOR_DECOMPOSEUR = (51, 0, 209)
COLOR_HERBIVORE = (225, 225, 0)
COLOR_DEAD = (47, 79, 79)

file = open('scenario.json')
scenario = json.load(file)


class Body(object):
    def __init__(self, parent):
        self.position = Vector2(random.randint(0, core.WINDOW_SIZE[0]), random.randint(0, core.WINDOW_SIZE[1]))
        self.parent = parent
        self.color = COLOR_HERBIVORE
        self.fustrum = Fustrum(150, self)
        self.acc = Vector2()
        self.mass = 9
        self.vitesse = Vector2()
        self.vMax = None
        self.accMax = None
        self.jaugeFaim = None
        self.jaugeFatigue = None
        self.jaugeProduction = None
        self.esperanceVie = None
        self.date_naissance = datetime.now()
        self.timerEsperanceVie = time.get_ticks()
        self.timerDormir = 0
        self.timerFatigue = 0
        self.timerFaim = 0
        self.timerReproduction = 0
        self.timerAvantMortDeFaim = 0
        self.mort = False
        self.dort = False
        self.reproduction = False



    def get_parent(self):
        return super().parent

    def update(self):

        if self.parent.isHerbivore():
            self.vMax = scenario['Herbivore']['parametres']['vitesseMax'][1]
            self.accMax = scenario['Herbivore']['parametres']['accelerationMax'][1]
            self.jaugeFaim = scenario['Herbivore']['parametres']['jaugeFaim']
            self.jaugeFatigue = scenario['Herbivore']['parametres']['jaugeFatigue']
            self.jaugeProduction = scenario['Herbivore']['parametres']['jaugeProduction']
            self.esperanceVie = scenario['Herbivore']['parametres']['esperanceVie']

        elif self.parent.isCarnivore():
            self.vMax = scenario['Carnivore']['parametres']['vitesseMax'][1]
            self.accMax = scenario['Carnivore']['parametres']['accelerationMax'][1]
            self.jaugeFaim = scenario['Carnivore']['parametres']['jaugeFaim']
            self.jaugeFatigue = scenario['Carnivore']['parametres']['jaugeFatigue']
            self.jaugeProduction = scenario['Carnivore']['parametres']['jaugeProduction']
            self.esperanceVie = scenario['Carnivore']['parametres']['esperanceVie']

        elif self.parent.isSuperpredateur():
            self.vMax = scenario['Superpredateur']['parametres']['vitesseMax'][1]
            self.accMax = scenario['Superpredateur']['parametres']['accelerationMax'][1]
            self.jaugeFaim = scenario['Superpredateur']['parametres']['jaugeFaim']
            self.jaugeFatigue = scenario['Superpredateur']['parametres']['jaugeFatigue']
            self.jaugeProduction = scenario['Superpredateur']['parametres']['jaugeProduction']
            self.esperanceVie = scenario['Superpredateur']['parametres']['esperanceVie']

        else:
            self.vMax = scenario['Decompositeur']['parametres']['vitesseMax'][1]
            self.accMax = scenario['Decompositeur']['parametres']['accelerationMax'][1]
            self.jaugeFaim = scenario['Decompositeur']['parametres']['jaugeFaim']
            self.jaugeFatigue = scenario['Decompositeur']['parametres']['jaugeFatigue']
            self.jaugeProduction = scenario['Decompositeur']['parametres']['jaugeProduction']
            self.esperanceVie = scenario['Decompositeur']['parametres']['esperanceVie']

        current_time = time.get_ticks()

        if not self.timerEsperanceVie == 0 and current_time - self.timerEsperanceVie > self.esperanceVie:
            self.die()
        else:
            self.timerEsperanceVie += current_time

        if not self.timerFatigue == 0 and current_time - self.timerFatigue > self.jaugeFatigue[1]:
            self.sleep()
        else:
            self.timerFatigue += current_time

        if not self.timerFaim and self.jaugeFaim[1] > current_time - self.timerFaim > self.jaugeFaim[0]:
            if self.isSleeping():
                self.wakeup()
        else:
            self.timerFaim += current_time

        if not self.timerFaim and current_time - self.timerFaim > self.jaugeFaim[1]:
            self.die()
        else:
            self.timerFaim += current_time

        if not self.timerReproduction and current_time - self.timerReproduction > self.jaugeProduction[1]:
            self.reproduct()
        else:
            self.timerReproduction += current_time

        if not self.isDead():
            if not self.isSleeping():
                if self.acc.length() > self.accMax / self.mass:
                    self.acc.scale_to_length(self.accMax / self.mass)

                self.vitesse = self.vitesse + self.acc

                if self.vitesse.length() > self.vMax:
                    self.vitesse.scale_to_length(self.vMax)

                self.position = self.position + self.vitesse

                self.acc = Vector2()
                self.edge()

    def show(self):
        if not self.isDead():
            if self.parent.isSuperpredateur():
                core.Draw.circle(COLOR_SUPERPREDATEUR, self.position, self.mass)
            elif self.parent.isCarnivore():
                core.Draw.circle(COLOR_CARNIVORE, self.position, self.mass-1)
            elif self.parent.isDecompositeur():
                core.Draw.circle(COLOR_DECOMPOSEUR, self.position, self.mass-2)
            else:
                core.Draw.circle(COLOR_HERBIVORE, self.position, self.mass-3)
        else:
            if self.parent.isSuperpredateur():
                core.Draw.circle(COLOR_DEAD, self.position, self.mass)
            elif self.parent.isCarnivore():
                core.Draw.circle(COLOR_DEAD, self.position, self.mass - 1)
            elif self.parent.isDecompositeur():
                core.Draw.circle(COLOR_DEAD, self.position, self.mass - 2)
            else:
                core.Draw.circle(COLOR_DEAD, self.position, self.mass - 3)


    def edge(self):
        if self.position.x <= self.mass:
            self.vitesse.x *= -1
        if self.position.x + self.mass >= core.WINDOW_SIZE[0]:
            self.vitesse.x *= -1
        if self.position.y <= self.mass:
            self.vitesse.y *= -1
        if self.position.y + self.mass >= core.WINDOW_SIZE[1]:
            self.vitesse.y *= -1

    def die(self):
        self.mort = True
        self.timerAvantMortDeFaim = 0
        self.timerFatigue = 0
        self.timerReproduction = 0
        self.timerEsperanceVie = 0
        self.timerDormir = 0

    def isDead(self):
        return self.mort

    def isSleeping(self):
        return self.dort

    def sleep(self):
        self.dort = True
        self.timerFatigue = 0

    def wakeup(self):
        self.dort = False

    def reproduct(self):
        self.reproduction = True
        self.timerReproduction = 0

    def isReproducting(self):
        return self.reproduction