import random
import sys

import pygame.display
from pygame.math import Vector2
import core
from agent import Agent
from body import Body
from creep import Creep
from obstacle import Obstacle


def setup():
    print("Setup START---------")

    pygame.display.set_caption("Test")

    core.fps = 30
    core.WINDOW_SIZE = [800, 800]
    core.fullscreen = True

    core.memory("agents", [])
    core.memory("obstacles", [])
    core.memory("creeps", [])

    addRandomly()

    print("Setup END-----------")


def computePerception(agent):
    """
    stocker les objets perçus dans la liste du fustrum de l'agent
    :param agent: agent concerné
    :return: liste des objets per_us du fustrum
    """
    agent.body.fustrum.perceptionList = list()
    for _agent in core.memory("agents"):
        if _agent.uuid != agent.uuid:
            if agent.body.fustrum.inside(_agent.body):
                agent.body.fustrum.perceptionList.append(_agent)
    for _obstacle in core.memory("obstacles"):
        if agent.body.fustrum.inside(_obstacle):
            agent.body.fustrum.perceptionList.append(_obstacle)
    for _creep in core.memory("creeps"):
        if agent.body.fustrum.inside(_creep):
            agent.body.fustrum.perceptionList.append(_creep)


def computeDecision(agent):
    """
    Mettre à jour la décision de l'agent
    :param agent: agent concerné
    """
    agent.update()


def applyDecision(agent):
    """
    Appliquer la décision de mouvement de l'agent
    :param agent: agent concerné
    """
    agent.body.update()


def draw():
    """
    Afficher des agents et des objets
    :return:
    """
    for agent in core.memory("agents"):
        agent.show()

    for obstacle in core.memory("obstacles"):
        obstacle.show()

    for creep in core.memory("creeps"):
        creep.show()


def addRandomly():
    """
    Ajouter aléatoirement les agents et les objets
    """
    # ajouter les agents
    for i in range(random.randint(0, 2), random.randint(4, 7)):
        core.memory("agents").append(Agent(Body()))

    # ajouter les obstacles
    for i in range(random.randint(0, 2), random.randint(3, 5)):
        core.memory("obstacles").append(Obstacle())

    # ajouter les creeps
    for i in range(random.randint(3, 5), random.randint(20, 50)):
        core.memory("creeps").append(Creep())


def updateEnv():
    for a in core.memory("agents"):
        for c in core.memory('creeps'):
            if a.body.position.distance_to(c.position) <= a.body.mass:
                c.position = Vector2(random.randint(0, core.WINDOW_SIZE[0]), random.randint(0, core.WINDOW_SIZE[1]))
                c.color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
                a.body.mass += 1
    for a in core.memory("agents"):
        for c in core.memory('obstacles'):
            if a.body.position.distance_to(c.position) <= a.body.mass:
                core.memory("agents").remove(a)

    for a in core.memory("agents"):
        for c in core.memory('agents'):
            if c.uuid != a.uuid:
                if a.body.position.distance_to(c.body.position) <= a.body.mass + c.body.mass:
                    if a.body.mass < c.body.mass:
                        c.body.mass += a.body.mass / 2
                        core.memory("agents").remove(a)
                    else:
                        a.body.mass += c.body.mass / 2
                        core.memory("agents").remove(c)


def addItemWithKeyboard():
    """
    Ajouter les agents et objets en utilisant les touches
    """
    if core.getKeyPressList("p"):
        core.memory("agents").append(Agent(Body()))

    if core.getKeyPressList("o"):
        core.memory("obstacles").append(Obstacle())

    if core.getKeyPressList("c"):
        core.memory("creeps").append(Creep())


def reset():
    core.memory("agents", [])
    core.memory("obstacles", [])
    core.memory("creeps", [])

    addRandomly()


def run():
    if core.getKeyPressList("r"):
        core.cleanScreen()
        reset()

    core.cleanScreen()

    if core.getKeyPressList("ESCAPE"):
        sys.exit()

    # Display
    draw()

    # Add items using keyboard
    addItemWithKeyboard()

    for agent in core.memory("agents"):
        computePerception(agent)

    for agent in core.memory("agents"):
        computeDecision(agent)

    for agent in core.memory("agents"):
        applyDecision(agent)

    updateEnv()


core.main(setup, run)
