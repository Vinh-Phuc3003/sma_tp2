import random
from pygame import Vector2, time
from body import Body
from epidemie import PARAMS

SAIN = "S"
INFECTE = "I"
GUERI = "R"  # ou immunisé contre réinfection
DECEDE = "D"


class Agent(object):
    def __init__(self):
        self.body = Body(self)
        self.statut = SAIN
        self.uuid = random.randint(100000, 999999999)

    def filtrePerception(self):
        """
        Filter les agents infectés perçus
        :return: liste des agents infectés
        """
        infecte = []

        for i in self.body.fustrum.perceptionList:
            if isinstance(i, Agent):
                i.dist = self.body.position.distance_to(i.body.position)

                if i.isInfected():
                    infecte.append(i)

        infecte.sort(key=lambda x: x.dist, reverse=False)

        return infecte

    def update(self):
        if not self.isDead():
            self.body.update()
            self.move()

            infecte = self.filtrePerception()

            # si l'agent est sain
            if self.isHealthy():
                contamination_risk = False
                # s'il existe des individus infectés à proximité,
                if len(infecte) > 0:
                    for agent in infecte:
                        # et que parmi eux il y a un individu contaminé,
                        if agent.body.contagieux:
                            # le risque de contamination est vrai.
                            contamination_risk = True
                    # s'il existe un risque de contamination et que l'agent n'a pas de chance,
                    if contamination_risk and random.random() < PARAMS["pourcentage_contagion"]:
                        # il est cas contact
                        self.infected()

    def show(self):
        self.body.show()

    def infected(self):
        """
        Agent est cas contact (mais pas contagieux ni symptomatique)
        """
        self.statut = INFECTE
        self.body.incubationTimer = time.get_ticks()
        self.body.timeBeforeContagionTimer = time.get_ticks()

    def infectedAndContagious(self):
        """
        Agent est contagieux (mais sans symptômes, car la durée d'incubation n'est pas terminée)
        """
        self.body.contagieux = True
        self.body.timeBeforeContagionTimer = 0

    def infectedWithSymptoms(self):
        """
        Agent a des symptômes, c'est à partir de ce moment là qu'il peut être guéri ou non
        """
        self.body.incubationTimer = 0
        self.body.symptoms = True
        if random.random() <= PARAMS["pourcentage_mortalite"]:
            self.body.deceaseTimer = time.get_ticks()
        else:
            self.body.healTimer = time.get_ticks()

    def healed(self):
        self.statut = GUERI
        self.body.healTimer = 0
        self.body.contagieux = False
        self.body.symptoms = False

    def dead(self):
        self.statut = DECEDE
        self.body.deceaseTimer = 0
        self.body.contagieux = False
        self.body.symptoms = False

    def isHealthy(self):
        return self.statut == SAIN

    def isInfected(self):
        return self.statut == INFECTE

    def isHealed(self):
        return self.statut == GUERI

    def isDead(self):
        return self.statut == DECEDE

    def move(self):
        self.body.acc += Vector2(random.randint(-1, 1), random.randint(-1, 1))
