PARAMS = {
    "duree_incubation": 6000,  # ms
    "duree_avant_contagion": 4000,  # ms
    "pourcentage_contagion": .8,
    "duree_avant_deces": 10000,  # ms
    "duree_avant_gueri": 8000,  # ms
    "pourcentage_mortalite": .1,
    "distance_mini_contagion": 30
}
