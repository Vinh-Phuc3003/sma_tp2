from random import sample
import sys
import core
from pygame import mouse
from agent import Agent


def setup():
    print("Setup START---------")

    core.fps = 30
    core.WINDOW_SIZE = [650, 650]
    # core.fullscreen = True

    core.memory("agents", [])
    addRandomly()

    print("Setup END-----------")


def computePerception(agent):
    """
    stocker les objets perçus dans la liste du fustrum de l'agent
    :param agent: agent concerné
    :return: liste des objets per_us du fustrum
    """
    agent.body.fustrum.perceptionList = list()
    for _agent in core.memory("agents"):
        if _agent.uuid != agent.uuid:
            if agent.body.fustrum.inside(_agent.body):
                agent.body.fustrum.perceptionList.append(_agent)


def computeDecision(agent):
    """
    Mettre à jour la décision de l'agent
    :param agent: agent concerné
    """
    agent.update()


def applyDecision(agent):
    """
    Appliquer la décision de mouvement de l'agent
    :param agent: agent concerné
    """
    agent.body.update()


def draw():
    """
    Afficher des agents et des objets
    """
    for agent in core.memory("agents"):
        agent.show()


def addRandomly():
    """
    Ajouter aléatoirement les agents et les objets
    """
    # ajouter les agents sains
    for i in range(40, 80):
        core.memory("agents").append(Agent())

    # choisir aléatoirement 4 agents pour les "rendre infectés"
    for agent in sample(core.memory("agents"), 4):
        agent.infected()


def updateEnv():
    for a in core.memory("agents"):
        a.update()


def addItemWithKeyboard():
    """
    Ajouter les agents et objets en utilisant les touches
    """
    # Taper "s" => ajouter des agents sains
    if core.getKeyPressList("s"):
        core.memory("agents").append(Agent())


def reset():
    core.memory("agents", [])
    addRandomly()


def agentInfected():
    """
    Déclencher d'épidémie : en cliquant avec la souris,
    l'agent le plus proche devient automatiquement "I",
    sauf qu'il est guéri (immunisé contre réinfection) ou mort
    :return:
    """
    best_agent = 0
    best_distance = 100000
    for agent in core.memory("agents"):
        if best_distance >= agent.body.position.distance_to(mouse.get_pos()) and not (agent.isDead() or agent.isHealed()):
            best_agent = agent
            best_distance = agent.body.position.distance_to(mouse.get_pos())
    best_agent.infected()

def run():
    if core.getKeyPressList("r"):
        reset()

    core.cleanScreen()

    if core.getKeyPressList("ESCAPE"):
        sys.exit()

    if core.getMouseLeftClick():
        agentInfected()

    # Display
    draw()

    # Add items using keyboard
    addItemWithKeyboard()

    for agent in core.memory("agents"):
        computePerception(agent)

    for agent in core.memory("agents"):
        computeDecision(agent)

    for agent in core.memory("agents"):
        applyDecision(agent)

    updateEnv()


core.main(setup, run)
