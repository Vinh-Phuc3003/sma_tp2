import random
from pygame import Vector2, time
from fustrum import Fustrum
import core
from epidemie import PARAMS

COLOR_SAIN = (169, 169, 169)
COLOR_INFECTE = (220, 20, 60)
COLOR_GUERI = (50, 205, 50)
COLOR_DECEDE = (51, 0, 209)


class Body(object):
    def __init__(self, parent):
        self.position = Vector2(random.randint(0, core.WINDOW_SIZE[0]), random.randint(0, core.WINDOW_SIZE[1]))
        self.parent = parent
        self.vitesse = Vector2()
        self.vMax = 2
        self.accMax = 10
        self.mass = 8
        self.color = COLOR_SAIN
        self.fustrum = Fustrum(PARAMS["distance_mini_contagion"], self)
        self.acc = Vector2()
        self.contagieux = False
        self.symptoms = False
        self.incubationTimer = 0
        self.timeBeforeContagionTimer = 0
        self.deceaseTimer = 0
        self.healTimer = 0

    def update(self):
        if not self.parent.isDead():
            current_time = time.get_ticks()

            # si l'agent est cas contact mais pas contagieux ni symtômatique
            if self.parent.isInfected() and not self.isContagious() and not self.hasSymptoms():
                # on observe la durée avant contagion, si elle s'est écoulée, l'agent devient contagieux
                if not self.timeBeforeContagionTimer == 0 and current_time - self.timeBeforeContagionTimer > PARAMS["duree_avant_contagion"]:
                    self.parent.infectedAndContagious()

            # si l'agent est contagieux mais pas symtômatique
            if self.parent.isInfected() and self.isContagious() and not self.hasSymptoms():
                # on observe la durée d'incubation, si celle-ci s'est écoulée, l'agent a des symptômes
                if not self.incubationTimer == 0 and current_time - self.incubationTimer > PARAMS["duree_incubation"]:
                    self.parent.infectedWithSymptoms()

            # si l'agent a des symptômes
            if self.hasSymptoms():
                # on observe s'il peut être guéri ou non
                if not self.deceaseTimer == 0 and current_time - self.deceaseTimer > PARAMS["duree_avant_deces"]:
                    self.parent.dead()
                if not self.healTimer == 0 and current_time - self.healTimer > PARAMS["duree_avant_gueri"]:
                    self.parent.healed()


            if self.acc.length() > self.accMax/self.mass:
                self.acc.scale_to_length(self.accMax/self.mass)

            self.vitesse = self.vitesse + self.acc

            if self.vitesse.length() > self.vMax:
                self.vitesse.scale_to_length(self.vMax)

            self.position = self.position + self.vitesse

            self.acc = Vector2()
            self.edge()
        else:
            self.acc = 0
            self.vitesse = 0

    def show(self):
        if self.parent.statut == "S":
            core.Draw.circle(COLOR_SAIN, self.position, self.mass)
        elif self.parent.statut == "I":
            core.Draw.circle(COLOR_INFECTE, self.position, self.mass)
        elif self.parent.statut == "R":
            core.Draw.circle(COLOR_GUERI, self.position, self.mass)
        else:
            core.Draw.circle(COLOR_DECEDE, self.position, self.mass)

    def edge(self):
        if self.position.x <= self.mass:
            self.vitesse.x *= -1
        if self.position.x+self.mass >= core.WINDOW_SIZE[0]:
            self.vitesse.x *= -1
        if self.position.y <= self.mass:
            self.vitesse.y *= -1
        if self.position.y + self.mass >= core.WINDOW_SIZE[1]:
            self.vitesse.y *= -1

    def isContagious(self):
        return self.contagieux

    def hasSymptoms(self):
        return self.symptoms
