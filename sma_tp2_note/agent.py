import random
from pygame import Vector2

from body import Body
from item import Vegetal

SUPERPREDATEUR = "s"
CARNIVORE = "c"
HERBIBORE = "h"
DECOMPOSEUR = "d"


class Agent(object):
    def __init__(self, statut):
        self.body = Body(self)
        self.statut = statut
        self.uuid = random.randint(100000, 999999999)
        self.coefPredator = 100
        self.coefFood = .01

    def filtrePerception(self):
        """
        Filter les agents infectés perçus
        :return: liste des agents infectés
        """
        eat = []
        meat = []
        veggie = []
        predators = []
        danger = []

        for i in self.body.fustrum.perceptionList:
            # cas superprédateur
            if self.isSuperpredateur():
                if isinstance(i, Agent) and i.isCarnivore():
                    i.dist = self.body.position.distance_to(i.body.position)
                    meat.append(i)
                if isinstance(i, Body):
                    i.dist = self.body.position.distance_to(i.position)
                    if i.parent.isCarnivore():
                        eat.append(i)

            # cas carnivore
            elif self.isCarnivore():
                if isinstance(i, Agent):
                    i.dist = self.body.position.distance_to(i.body.position)
                    if i.isHerbivore():
                        meat.append(i)
                    if i.isSuperpredateur():
                        predators.append(i)
                if isinstance(i, Body):
                    if i.parent.isHerbivore():
                        eat.append(i)
                    if i.parent.isSuperpredateur():
                        danger.append(i)

            # cas herbivore
            elif self.isHerbivore():
                if isinstance(i, Vegetal):
                    i.dist = self.body.position.distance_to(i.position)
                    veggie.append(i)
                if isinstance(i, Body):
                    """if isinstance(i, Vegetal):
                        eat.append(i)"""
                    if i.parent.isCarnivore():
                        danger.append(i)
                if isinstance(i, Agent) and i.isCarnivore():
                    i.dist = self.body.position.distance_to(i.body.position)
                    predators.append(i)

            # cas décompositeur
            else:
                if isinstance(i, Agent) and i.body.isDead():
                    i.dist = self.body.position.distance_to(i.body.position)
                    meat.append(i)
                if isinstance(i, Body) and i.isDead():
                    i.dist = self.body.position.distance_to(i.position)
                    eat.append(i)

        eat.sort(key=lambda x: x.dist, reverse=False)
        meat.sort(key=lambda x: x.dist, reverse=False)
        veggie.sort(key=lambda x: x.dist, reverse=False)
        predators.sort(key=lambda x: x.dist, reverse=False)
        danger.sort(key=lambda x: x.dist, reverse=False)

        return eat, meat, veggie, predators, danger

    def update(self):
        self.body.update()
        self.move()

        eat, meat, veggie, predators, danger = self.filtrePerception()

        if len(meat) > 0:
            target = meat[0].body.position - self.body.position
        else:
            target = Vector2(random.randint(-1, 1), random.randint(-1, 1))
            while target.length() == 0:
                target = Vector2(random.randint(-1, 1), random.randint(-1, 1))
        target.scale_to_length(target.length() * self.coefFood)
        self.body.acc += target

        if len(veggie) > 0:
            target = veggie[0].position - self.body.position
        else:
            target = Vector2(random.randint(-1, 1), random.randint(-1, 1))
            while target.length() == 0:
                target = Vector2(random.randint(-1, 1), random.randint(-1, 1))
        target.scale_to_length(target.length() * self.coefFood)
        self.body.acc += target

        if len(eat) > 0:
            target = eat[0].position - self.body.position
            target.scale_to_length(target.length() * self.coefFood)
            self.body.acc += target

        if len(predators) > 0:
            target = self.body.position - predators[0].body.position
            target.scale_to_length(1 / target.length() ** 2)
            target.scale_to_length(target.length() * (self.coefPredator+self.body.mass))
            self.body.acc = self.body.acc + target

        if len(danger) > 0:
            target = self.body.position - danger[0].position
            target.scale_to_length(1 / target.length() ** 2)
            target.scale_to_length(target.length() * (self.coefPredator + self.body.mass))
            self.body.acc = self.body.acc + target

    def show(self):
        self.body.show()

    def move(self):
        self.body.acc += Vector2(random.randint(-1, 1), random.randint(-1, 1))

    def isSuperpredateur(self):
        return self.statut == 's'

    def isCarnivore(self):
        return self.statut == 'c'

    def isHerbivore(self):
        return self.statut == 'h'

    def isDecompositeur(self):
        return self.statut == 'd'
