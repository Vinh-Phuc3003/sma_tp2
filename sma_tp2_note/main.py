import random
from random import sample
import sys
import core
from pygame import mouse, Vector2
from agent import Agent
from item import Vegetal


def setup():
    print("Setup START---------")

    core.fps = 30
    core.WINDOW_SIZE = [700, 700]
    # core.fullscreen = True

    core.memory("agents", [])
    core.memory("items", [])

    addRandomly()

    print("Setup END-----------")


def computePerception(agent):
    """
    stocker les objets perçus dans la liste du fustrum de l'agent
    :param agent: agent concerné
    :return: liste des objets per_us du fustrum
    """
    agent.body.fustrum.perceptionList = list()
    for _agent in core.memory("agents"):
        if _agent.uuid != agent.uuid:
            if agent.body.fustrum.inside(_agent.body):
                agent.body.fustrum.perceptionList.append(_agent)

    for _vegetal in core.memory("items"):
        if agent.body.fustrum.inside(_vegetal):
            agent.body.fustrum.perceptionList.append(_vegetal)

def computeDecision(agent):
    """
    Mettre à jour la décision de l'agent
    :param agent: agent concerné
    """
    agent.update()


def applyDecision(agent):
    """
    Appliquer la décision de mouvement de l'agent
    :param agent: agent concerné
    """
    agent.body.update()


def draw():
    """
    Afficher des agents et des objets
    """
    for s in core.memory("agents"):
        s.show()
    for v in core.memory("items"):
        v.show()


def addRandomly():
    """
    Ajouter aléatoirement les agents et les objets
    """
    # ajouter les superprédateurs
    for i in range(0, random.randint(2, 5)):
        core.memory("agents").append(Agent("s"))

    # ajouter les carnivores
    for i in range(0, random.randint(10, 20)):
        core.memory("agents").append(Agent("c"))

    # ajouter les décomposeurs
    for i in range(0, random.randint(5, 9)):
        core.memory("agents").append(Agent("d"))

    # ajouter les végétaux
    for i in range(0, random.randint(60, 80)):
        core.memory("items").append(Vegetal())

    # ajouter les herbivores
    for i in range(0, random.randint(16, 20)):
        core.memory("agents").append(Agent("h"))


def updateEnv():
    for agent in core.memory("agents"):
        if agent.isHerbivore():
            for vegetal in core.memory("items"):
                if agent.body.position.distance_to(vegetal.position) <= agent.body.mass:
                    core.memory("items").remove(vegetal)
        else:
            for other_agent in core.memory("agents"):
                if agent.uuid != other_agent.uuid:
                    if agent.body.position.distance_to(other_agent.body.position) <= agent.body.mass + other_agent.body.mass:
                        if agent.isSuperpredateur() and other_agent.isCarnivore():
                            core.memory("agents").remove(other_agent)
                        if agent.isCarnivore()and other_agent.isHerbivore():
                            core.memory("agents").remove(other_agent)
                        if agent.isDecompositeur() and other_agent.body.isDead():
                            core.memory("agents").remove(other_agent)


def reset():
    core.memory("agents", [])
    core.memory("items", [])
    addRandomly()


def run():
    if core.getKeyPressList("r"):
        reset()

    core.cleanScreen()

    if core.getKeyPressList("ESCAPE"):
        sys.exit()

    # Display
    draw()

    for agent in core.memory("agents"):
        if agent.body.isReproducting():
            if agent.body.isSuperpredateur():
                core.memory("agents").append(Agent("s"))
            elif agent.body.isCarnivore():
                core.memory("agents").append(Agent("c"))
            elif agent.body.isHerbivore():
                core.memory("agents").append(Agent("h"))
            else:
                core.memory("agents").append(Agent("d"))

            agent.body.reproduction = False
            agent.body.timerReproduction = 0



    for agent in core.memory("agents"):
        computePerception(agent)

    for agent in core.memory("agents"):
        computeDecision(agent)

    for agent in core.memory("agents"):
        applyDecision(agent)

    updateEnv()


core.main(setup, run)
